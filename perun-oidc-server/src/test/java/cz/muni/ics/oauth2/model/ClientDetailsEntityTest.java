/*******************************************************************************
 * Copyright 2018 The MIT Internet Trust Consortium
 *
 * Portions copyright 2011-2013 The MITRE Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/**
 *
 */
package cz.muni.ics.oauth2.model;

import com.google.common.collect.ImmutableSet;
import cz.muni.ics.oauth2.model.enums.AuthMethod;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author jricher
 *
 */
public class ClientDetailsEntityTest {

	/**
	 * Test method for {@link MockOidcClientDetailsEntity#MockOidcClientDetailsEntity()}.
	 */
	@Test
	public void testClientDetailsEntity() {
		MockOidcClientDetailsEntity c = new MockOidcClientDetailsEntity();

		c.setClientId("s6BhdRkqt3");
		c.setClientSecret("ZJYCqe3GGRvdrudKyZS0XhGv_Z45DuKhCUk0gBR1vZk");
		c.setRedirectUris(ImmutableSet.of("https://client.example.org/callback", "https://client.example.org/callback2"));
		c.setClientName("My Example");
		c.setTokenEndpointAuthMethod(AuthMethod.SECRET_BASIC.getValue());
		c.setAccessTokenValiditySeconds(600);

		assertEquals("s6BhdRkqt3", c.getClientId());
		assertEquals("ZJYCqe3GGRvdrudKyZS0XhGv_Z45DuKhCUk0gBR1vZk", c.getClientSecret());
		assertEquals(ImmutableSet.of("https://client.example.org/callback", "https://client.example.org/callback2"), c.getRedirectUris());
		assertEquals("My Example", c.getClientName());
		assertEquals(AuthMethod.SECRET_BASIC, c.getTokenEndpointAuthMethod());
		assertEquals(600, c.getAccessTokenValiditySeconds().intValue());
	}

}
