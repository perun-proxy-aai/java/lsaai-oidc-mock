package cz.muni.ics.oidc.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

/**
 * A custom scheduler for tasks with usage of ShedLock.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Configuration
@EnableScheduling
@Slf4j
public class CustomTaskScheduler {

	private static final long ONE_MINUTE = 60000L;

	private final CustomClearTasks customClearTasks;

	@Autowired
	public CustomTaskScheduler(CustomClearTasks customClearTasks)
	{
		this.customClearTasks = customClearTasks;
	}

	@Transactional(value = "defaultTransactionManager")
	@Scheduled(fixedDelay = 60 * ONE_MINUTE, initialDelay = ONE_MINUTE)
	public void clearExpiredSites() {
		long start = System.currentTimeMillis();
		int count = this.customClearTasks.clearExpiredSites(TimeUnit.MINUTES.toMillis(15));
		long execution = System.currentTimeMillis() - start;
		log.info("clearExpiredSites took {}ms, deleted {} records", execution, count);
	}

	@Transactional(value = "defaultTransactionManager")
	@Scheduled(fixedDelay = 60 * ONE_MINUTE, initialDelay = 12 * ONE_MINUTE)
	public void clearExpiredTokens() {
		long start = System.currentTimeMillis();
		int count = this.customClearTasks.clearExpiredTokens(TimeUnit.MINUTES.toMillis(15));
		long execution = System.currentTimeMillis() - start;
		log.info("clearExpiredTokens took {}ms, deleted {} records", execution, count);
	}

	@Transactional(value = "defaultTransactionManager")
	@Scheduled(fixedDelay = 60 * ONE_MINUTE, initialDelay = 24 * ONE_MINUTE)
	public void clearExpiredAuthorizationCodes() {
		long start = System.currentTimeMillis();
		int count = this.customClearTasks.clearExpiredAuthorizationCodes(TimeUnit.MINUTES.toMillis(15));
		long execution = System.currentTimeMillis() - start;
		log.info("clearExpiredAuthorizationCodes took {}ms, deleted {} records", execution, count);
	}

	@Transactional(value = "defaultTransactionManager")
	@Scheduled(fixedDelay = 60 * ONE_MINUTE, initialDelay = 36 * ONE_MINUTE)
	public void clearExpiredDeviceCodes() {
		long start = System.currentTimeMillis();
		int count = this.customClearTasks.clearExpiredDeviceCodes(TimeUnit.MINUTES.toMillis(15));
		long execution = System.currentTimeMillis() - start;
		log.info("clearExpiredDeviceCodes took {}ms, deleted {} records", execution, count);
	}

	@Transactional(value = "defaultTransactionManager")
	@Scheduled(fixedDelay = 60 * ONE_MINUTE, initialDelay = 48 * ONE_MINUTE)
	public void clearOrphanedSavedUserAuths() {
		long start = System.currentTimeMillis();
		int count = this.customClearTasks.clearOrphanedSavedUserAuths(TimeUnit.MINUTES.toMillis(15));
		long execution = System.currentTimeMillis() - start;
		log.info("clearOrphanedSavedUserAuths took {}ms, deleted {} records", execution, count);
	}

}
