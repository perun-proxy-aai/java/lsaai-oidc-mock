/*******************************************************************************
 * Copyright 2018 The MIT Internet Trust Consortium
 *
 * Portions copyright 2011-2013 The MITRE Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.muni.ics.openid.connect.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.ics.data.UserInfoInitializer;
import cz.muni.ics.localauth.SelectedUserDetails;
import cz.muni.ics.oauth2.model.MockOidcClientDetailsEntity;
import cz.muni.ics.oauth2.service.ClientDetailsEntityService;
import cz.muni.ics.oidc.server.configurations.Ga4ghBrokerConfig;
import cz.muni.ics.oidc.server.ga4gh.Ga4ghApiClaimSource;
import cz.muni.ics.openid.connect.model.MockOidcUserInfo;
import cz.muni.ics.openid.connect.model.UserInfo;
import cz.muni.ics.openid.connect.service.UserInfoService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of the UserInfoService
 *
 * @author Michael Joseph Walsh, jricher
 *
 */
@Slf4j
@Component("userInfoService")
public class OidcMockUserInfoService implements UserInfoService {

	private final Map<String, MockOidcUserInfo> userInfoMap;

	private final ClientDetailsEntityService clientService;

	private final Ga4ghApiClaimSource ga4ghApiClaimSource;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	public OidcMockUserInfoService(@NonNull UserInfoInitializer initializer,
								   @NonNull Ga4ghBrokerConfig ga4ghBrokerConfig,
								   @NonNull ClientDetailsEntityService clientService) {
		this.userInfoMap = new HashMap<>();
		for (MockOidcUserInfo userInfo: initializer.initialize()) {
			log.info("Loaded userinfo object: {}", userInfo);
			userInfoMap.put(userInfo.getSub(), userInfo);
		}
		this.ga4ghApiClaimSource = new Ga4ghApiClaimSource(ga4ghBrokerConfig);
		this.clientService = clientService;
	}

	private MockOidcUserInfo getFromMap(@NonNull String username) {
		MockOidcUserInfo original = userInfoMap.getOrDefault(username, null);
		if (original == null) {
			return null;
		}
		try {
			return objectMapper.readValue(objectMapper.writeValueAsString(original), MockOidcUserInfo.class);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public UserInfo get(@NonNull String username, @NonNull String clientId, @NonNull Set<String> scope) {
		MockOidcClientDetailsEntity client = clientService.loadClientByClientId(clientId);
		if (client == null) {
			return null;
		}
		MockOidcUserInfo userInfo = getFromMap(username);
		if (userInfo == null) {
			return null;
		}
		if (scope.contains("ga4gh_passport_v1")) {
			Set<String> visas = userInfo.getGa4ghPassportV1();
			Set<String> brokerVisas = ga4ghApiClaimSource.produceValue(userInfo.getSub());
			if (visas == null) {
				visas = brokerVisas;
			} else {
				if (brokerVisas != null) {
					visas.addAll(brokerVisas);
				}
			}
			userInfo.setGa4ghPassportV1(visas);
		}
		return userInfo;
	}

	@Override
	public SelectedUserDetails get(@NonNull String username) {
		MockOidcUserInfo userInfo = userInfoMap.getOrDefault(username, null);
		log.info("Loading user by username {} - found {}", username, userInfo);
		if (userInfo == null) {
			return null;
		}
		return new SelectedUserDetails(userInfo.getSub(), userInfo.getWebDisplayName());
	}

	@Override
	public List<SelectedUserDetails> getAllForWeb() {
		List<SelectedUserDetails> users = userInfoMap.values().stream()
				.map(userInfo -> new SelectedUserDetails(userInfo.getSub(), userInfo.getWebDisplayName()))
				.collect(Collectors.toList());
		log.info("Loading user for web display - found {}", users);
		return users;
	}

}
