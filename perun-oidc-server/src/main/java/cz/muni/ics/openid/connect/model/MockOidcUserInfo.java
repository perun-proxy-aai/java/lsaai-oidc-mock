package cz.muni.ics.openid.connect.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MockOidcUserInfo implements UserInfo {

	private final String profile = null;

	private final String picture = null;

	private final String website = null;

	private final boolean emailVerified = true;

	private final String gender = null;

	private final String phoneNumber = null;

	private final Boolean phoneNumberVerified = null;

	private final DefaultAddress address = null;

	private final String updatedTime = null;

	private final String birthdate = null;

	// updatable

	@JsonAlias("web-display-name")
	private String webDisplayName;

	@JsonAlias("sub")
	private String sub;

	@JsonAlias("preferred-username")
	private String preferredUsername;

	@JsonAlias("name")
	private String name;

	@JsonAlias("given-name")
	private String givenName;

	@JsonAlias("family-name")
	private String familyName;

	@JsonAlias("middle-name")
	private String middleName;

	@JsonAlias("nickname")
	private String nickname;

	@JsonAlias("email")
	private String email;

	@JsonAlias("zone-info")
	private String zoneinfo;

	@JsonAlias("locale")
	private String locale;

	@JsonAlias("country")
	private String country;

	@JsonAlias("eduperson-entitlement")
	private Set<String> eduPersonEntitlement;

	@JsonAlias("voperson-external-affiliation")
	private Set<String> voPersonExternalAffiliation;

	@JsonAlias("ga4gh-passport-v1")
	private Set<String> ga4ghPassportV1;

	@JsonAlias("eduperson-orcid")
	private String eduPersonOrcid;

	@JsonAlias("schac-home-organization")
	private Set<String> schacHomeOrganization;

	@JsonAlias("eduperson-scoped-affiliation")
	private Set<String> eduPersonScopedAffiliation;

	@JsonAlias("voperson-current-external-affiliation")
	private Set<String> voPersonCurrentExternalAffiliation;

	@JsonAlias("authenticating-entity")
	private String authenticatingEntity;

	@JsonAlias("authn-instant")
	private String authnInstant;

	@JsonAlias("acr")
	private String acr;

	@Override
	public JsonObject toJson() {
		JsonObject obj = new JsonObject();
		//openid
		obj.addProperty("sub", this.getSub());
		//profile
		obj.addProperty("name", this.getName());
		obj.addProperty("preferred_username", this.getPreferredUsername());
		obj.addProperty("given_name", this.getGivenName());
		obj.addProperty("family_name", this.getFamilyName());
		obj.addProperty("middle_name", this.getMiddleName());
		obj.addProperty("nickname", this.getNickname());
		obj.addProperty("zoneinfo", this.getZoneinfo());
		obj.addProperty("locale", this.getLocale());
		obj.addProperty("updated_at", this.getUpdatedTime());
		obj.addProperty("birthdate", this.getBirthdate());
		//email
		obj.addProperty("email", this.getEmail());
		obj.addProperty("email_verified", this.getEmailVerified());

		obj.addProperty("country", getCountry());

		obj.add("eduperson_entitlement", toArray(getEduPersonEntitlement()));

		obj.add("voperson_external_affiliation", toArray(getVoPersonExternalAffiliation()));

		obj.add("ga4gh_passport_v1", toArray(getGa4ghPassportV1()));

		obj.addProperty("eduperson_orcid", getEduPersonOrcid());

		obj.add("schac_home_organization", toArray(getSchacHomeOrganization()));

		obj.add(" eduperson_scoped_affiliation", toArray(getEduPersonScopedAffiliation()));

		obj.add(" voperson_current_external_affiliation ", toArray(getVoPersonCurrentExternalAffiliation()));

		//authenticating_entity
		obj.addProperty("authenticating_entity", getAuthenticatingEntity());

		//authn_details
		obj.addProperty("authn_details", getAcr());
		obj.addProperty("authn_details", getAuthnInstant());
		return obj;
	}

	private JsonElement toArray(Set<String> obj) {
		if (obj == null || obj.isEmpty()) {
			return null;
		}
		JsonArray arr = new JsonArray();
		for (String object: obj) {
			arr.add(object);
		}
		return arr;
	}

	// conformance methods with no effect
	@Override
	public void setAddress(Address address) {
	}

	@Override
	public void setProfile(String profile) {

	}

	@Override
	public void setPicture(String picture) {

	}

	@Override
	public void setWebsite(String website) {

	}

	@Override
	public Boolean getEmailVerified() {
		return null;
	}

	@Override
	public void setEmailVerified(Boolean emailVerified) {

	}

	@Override
	public void setGender(String gender) {

	}

	@Override
	public void setPhoneNumber(String phoneNumber) {

	}

	@Override
	public void setPhoneNumberVerified(Boolean phoneNumberVerified) {

	}

	@Override
	public void setUpdatedTime(String updatedTime) {

	}

	@Override
	public void setBirthdate(String birthdate) {

	}

	@Override
	@JsonIgnore
	public JsonObject getSource() {
		return null;
	}
}
