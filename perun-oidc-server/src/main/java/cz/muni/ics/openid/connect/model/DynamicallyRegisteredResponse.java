package cz.muni.ics.openid.connect.model;

import cz.muni.ics.oauth2.model.MockOidcClientDetailsEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class DynamicallyRegisteredResponse extends DynamicallyRegisteredRequestBody {

    private String clientId;

    private String clientSecret;

    private String registrationAccessToken;
    
    private String registrationClientUri;

    private Long clientIdIssuedAt;

    private Long clientSecretExpiresAt;

    public DynamicallyRegisteredResponse(
            MockOidcClientDetailsEntity registeredClient,
            String registrationAccessToken,
            String registrationClientUri,
            Long clientIdIssuedAt,
            Long clientSecretExpiresAt
    ) {
        super();
        this.setClientId(registeredClient.getClientId());
        this.setClientSecret(registeredClient.getClientSecret());
        this.setRegistrationAccessToken(registrationAccessToken);
        this.setRegistrationClientUri(registrationClientUri);
        this.setClientIdIssuedAt(clientIdIssuedAt);
        this.setClientSecretExpiresAt(clientSecretExpiresAt);
        mapFromClient(registeredClient);
    }

    private void mapFromClient(MockOidcClientDetailsEntity registeredClient) {
        this.setClientName(registeredClient.getClientName());
        this.setRedirectUris(registeredClient.getRedirectUris());
        if (registeredClient.getTokenEndpointAuthMethod() != null) {
            this.setTokenEndpointAuthMethod(registeredClient.getTokenEndpointAuthMethod().getValue());
        } else {
            this.setTokenEndpointAuthMethod(null);
        }
        this.setClientSecret(clientSecret);
        this.setScope(registeredClient.getScope());
        this.setGrantTypes(registeredClient.getGrantTypes());
        this.setPostLogoutRedirectUris(registeredClient.getPostLogoutRedirectUris());
        this.setAccessTokenValiditySeconds(registeredClient.getAccessTokenValiditySeconds());
        this.setRefreshTokenValiditySeconds(registeredClient.getRefreshTokenValiditySeconds());
        this.setResources(registeredClient.getResourceIds());
        this.setIdTokenValiditySeconds(registeredClient.getIdTokenValiditySeconds());
        this.setDeviceCodeValiditySeconds(registeredClient.getDeviceCodeValiditySeconds());
    }

}
