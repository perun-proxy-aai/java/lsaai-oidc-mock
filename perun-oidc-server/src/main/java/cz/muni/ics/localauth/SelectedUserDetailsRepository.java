package cz.muni.ics.localauth;

import java.util.List;

public interface SelectedUserDetailsRepository {

    SelectedUserDetails getByUsername(String username);

    List<SelectedUserDetails> getAllUsers();

}
