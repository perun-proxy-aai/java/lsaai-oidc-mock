package cz.muni.ics.localauth;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.security.Principal;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SelectedUserPrincipal implements Principal {

    private String username;

    private String acr;

    private long authTime;

    @Override
    public String getName() {
        return username;
    }
}
