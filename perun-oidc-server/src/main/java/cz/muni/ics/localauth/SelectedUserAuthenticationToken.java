package cz.muni.ics.localauth;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class SelectedUserAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public SelectedUserAuthenticationToken(Collection<? extends GrantedAuthority> authorities,
                                           SelectedUserPrincipal principal)
    {
        super(principal, null, authorities);
    }

}
