package cz.muni.ics.localauth;

import cz.muni.ics.openid.connect.service.impl.OidcMockUserInfoService;

import java.util.List;

public class SelectedUserDetailsRepositoryImpl implements SelectedUserDetailsRepository {

    private final OidcMockUserInfoService userInfoService;

    public SelectedUserDetailsRepositoryImpl(OidcMockUserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @Override
    public SelectedUserDetails getByUsername(String username) {
        return userInfoService.get(username);
    }

    @Override
    public List<SelectedUserDetails> getAllUsers() {
        return userInfoService.getAllForWeb();
    }
}
