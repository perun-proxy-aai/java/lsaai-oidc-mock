package cz.muni.ics.localauth;

import cz.muni.ics.oauth2.web.endpoint.AuthorizationEndpoint;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Filter to check client authentication via JWT Bearer assertions.
 *
 * @author jricher
 *
 */
public class PreselectedUserAuthenticationProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {

	public static final String PARAM_NAME = "username_hint";

	private final SelectedUserAuthenticationProvider authenticationProvider;

	protected PreselectedUserAuthenticationProcessingFilter(SelectedUserAuthenticationProvider authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
		this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(AuthorizationEndpoint.ENDPOINT_INIT_URL));
	}

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		String user = request.getParameter(PARAM_NAME);
		try {
			if (StringUtils.hasText(user)) {
				return authenticationProvider.authenticate(user);
			}
		} catch (AuthenticationException ignored) {
			// OK to pass, normal authentication filter will take care of
		}
		return null;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		return null;
	}

}
