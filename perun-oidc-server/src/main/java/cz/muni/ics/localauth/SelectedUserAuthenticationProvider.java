package cz.muni.ics.localauth;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.List;

@Slf4j
public class SelectedUserAuthenticationProvider implements AuthenticationProvider {

    private final SelectedUserDetailsService userDetailsService;

    public SelectedUserAuthenticationProvider(@NonNull SelectedUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return authenticate(authentication.getName());
     }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(SelectedUserAuthenticationToken.class)
                || authentication.isAssignableFrom(PreAuthenticatedAuthenticationToken.class);
    }

    public UsernamePasswordAuthenticationToken authenticate(String username) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (userDetails != null) {
            SelectedUserPrincipal principal = new SelectedUserPrincipal(userDetails.getUsername(), "",
                    System.currentTimeMillis());
            return new SelectedUserAuthenticationToken(List.of(new SimpleGrantedAuthority("ROLE_USER")), principal);
        }
        throw new BadCredentialsException("Username " + username + " not found");
    }
}
