package cz.muni.ics.data;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import cz.muni.ics.oauth2.model.MockOidcClientDetailsEntity;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClientDetailsInitializer {

    private final String path;

    public ClientDetailsInitializer(String directoryPath) {
        if (!StringUtils.hasText(directoryPath)) {
            throw new IllegalArgumentException("Empty path for userInfo files");
        }
        File f = new File(directoryPath);
        if (!f.exists()) {
            throw new IllegalArgumentException("Provided directory does not exist");
        } else if (!f.isDirectory()) {
            throw new IllegalArgumentException("Provided path is not a directory");
        }
        this.path = directoryPath;
    }

    public Collection<MockOidcClientDetailsEntity> initialize() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try (Stream<Path> paths = Files.walk(Paths.get(this.path))) {
            return paths.filter(Files::isRegularFile)
                    .map(path -> {
                        try {
                            return mapper.readValue(path.toFile(), MockOidcClientDetailsEntity.class);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }).collect(Collectors.toSet());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
