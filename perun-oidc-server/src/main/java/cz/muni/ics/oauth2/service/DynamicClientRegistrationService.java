package cz.muni.ics.oauth2.service;

import cz.muni.ics.oauth2.model.MockOidcClientDetailsEntity;
import cz.muni.ics.openid.connect.model.DynamicallyRegisteredRequestBody;

public interface DynamicClientRegistrationService {

    MockOidcClientDetailsEntity saveClient(String tokenClientId, DynamicallyRegisteredRequestBody requestedRegistration);

    void removeClient(String tokenClientId, String dynregClientId);

}
