/*******************************************************************************
 * Copyright 2018 The MIT Internet Trust Consortium
 *
 * Portions copyright 2011-2013 The MITRE Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package cz.muni.ics.oauth2.service.impl;

import cz.muni.ics.data.ClientDetailsInitializer;
import cz.muni.ics.oauth2.model.MockOidcClientDetailsEntity;
import cz.muni.ics.oauth2.service.ClientDetailsEntityService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
@Service("clientDetailsService")
public class MockOidcClientDetailsEntityService implements ClientDetailsEntityService {

	private final Map<String, MockOidcClientDetailsEntity> clientDetailsEntityMap;

	@Autowired
	public MockOidcClientDetailsEntityService(@NonNull ClientDetailsInitializer initializer) {
		this.clientDetailsEntityMap = new ConcurrentHashMap<>();
		for (MockOidcClientDetailsEntity client: initializer.initialize()) {
			log.info("Loaded client object: {}", client);
			this.clientDetailsEntityMap.put(client.getClientId(), client);
		}
	}

	@Override
	public MockOidcClientDetailsEntity loadClientByClientId(String clientId) throws OAuth2Exception {
		MockOidcClientDetailsEntity client = clientDetailsEntityMap.getOrDefault(clientId, null);
		if (client == null) {
			throw new InvalidClientException(clientId);
		}
		return client;
	}

	@Override
	public boolean checkResourceIdsAreAllowedForClient(String clientId, Set<String> resourceIds) {
		if (resourceIds == null || resourceIds.isEmpty()) {
			return true;
		}
		Collection<String> allowedResourceIds = getResourceIdsForClientID(clientId);
		if (allowedResourceIds == null || allowedResourceIds.isEmpty()) {
			return resourceIds.isEmpty();
		}
		return allowedResourceIds.containsAll(resourceIds);
	}

	@Override
	public Set<String> getMismatchedResourceIds(String clientId, Set<String> resourceIds) {
		if (resourceIds == null || resourceIds.isEmpty()) {
			return new HashSet<>();
		}
		Set<String> copy = new HashSet<>(resourceIds);
		Collection<String> allowedResourceIds = getResourceIdsForClientID(clientId);
		if (allowedResourceIds != null && !allowedResourceIds.isEmpty()) {
			copy.removeAll(resourceIds);
		}
		return copy;
	}

	private Collection<String> getResourceIdsForClientID(String clientId) {
		MockOidcClientDetailsEntity clientDetails = clientDetailsEntityMap.getOrDefault(clientId, null);
		if (clientDetails == null) {
			throw new InvalidClientException(clientId);
		}
		return clientDetails.getResourceIds();
	}

	@Override
	public MockOidcClientDetailsEntity saveClient(MockOidcClientDetailsEntity client) {
		String clientId = UUID.randomUUID().toString();
		while (clientDetailsEntityMap.containsKey(clientId)) {
			clientId = UUID.randomUUID().toString();
		}
		String clientSecret = String.format("%s-%s", UUID.randomUUID(), UUID.randomUUID());
		client.setClientId(clientId);
		client.setClientSecret(clientSecret);

		clientDetailsEntityMap.put(clientId, client);
		return client;
	}

	@Override
	public void removeClient(MockOidcClientDetailsEntity dynregClient) {
		String clientId = dynregClient.getClientId();

		Set<String> clientIdsToRemove = new HashSet<>();
		clientIdsToRemove.add(clientId);
		boolean found = true;
		Collection<MockOidcClientDetailsEntity> clients = clientDetailsEntityMap.values();
		while (found) {
			Set<String> childClients = clients.stream()
					.filter(client -> clientIdsToRemove.contains(client.getParentClientId()))
					.map(MockOidcClientDetailsEntity::getClientId)
					.filter(Objects::nonNull)
					.collect(Collectors.toSet());
			for (String cid: clientIdsToRemove) {
				clientDetailsEntityMap.remove(cid);
			}

			clientIdsToRemove.clear();
			clientIdsToRemove.addAll(childClients);

			if (childClients.isEmpty()) {
				found = false;
			}
		}
	}

}
