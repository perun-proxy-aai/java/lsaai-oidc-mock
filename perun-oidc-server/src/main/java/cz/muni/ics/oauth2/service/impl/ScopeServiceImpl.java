package cz.muni.ics.oauth2.service.impl;

import com.google.common.collect.Lists;
import cz.muni.ics.oauth2.service.ScopeService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ScopeServiceImpl implements ScopeService {

    private final Set<String> scopes;

    public ScopeServiceImpl() {
        this.scopes = new HashSet<>();
        scopes.add("openid");
        scopes.add("profile");
        scopes.add("email");
        scopes.add("address");
        scopes.add("phone");
        scopes.add("offline_access");
        scopes.add("country");
        scopes.add("eduperson_entitlement");
        scopes.add("voperson_external_affiliation");
        scopes.add("ga4gh_passport_v1");
        scopes.add("eduperson_orcid");
        scopes.add("schac_home_organization");
        scopes.add("eduperson_scoped_affiliation");
        scopes.add("voperson_current_external_affiliation");
        scopes.add("authenticating_entity");
        scopes.add("client_dynamic_registration");
        scopes.add("cleint_dynamic_update");
        scopes.add("client_dynamic_deregistration");
    }
    @Override
    public Set<String> getScopes() {
        return scopes;
    }
}
