<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <base href="${config.issuer}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="resources/lsaai/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="resources/lsaai/css/eduteams.css" rel="stylesheet" type="text/css"/>
    <link href="resources/lsaai/css/cmservice.css" rel="stylesheet" type="text/css"/>
    <title>LSAAI</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
<body>
<div class="row">
    <div class="offset-1 col-10 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6">
        <div class="card">
            <img class="card-img-top" src="resources/lsaai/img/lsaai_logo.png" alt="Life Science Login logo">
            <div class="card-body">